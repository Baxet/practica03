/**

* este programa  pide informacion al usuario , para guardarlos en la variable , para luego  presentarlos en formato de salida.
*
*
* @author Daniel Hern�mdez Esparza.
* @date 15 de Marzo del 2021.
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
  int datoentero;
/* se declara la variable  con int  para introducir y un valor entero*/

 printf("Escriba un Valor Entero");

 /* se pide al usuario el dato para ser almacenado */

 scanf("%d",&datoentero);

 /* se guarda el dato que le dio el usuario para su uso proximo */

printf("El valor entero es:%d\n",datoentero);

/* presentamos al usuario el valor guardado con anterioriad */

return 0;

/* se termina el programa normalmente */

}
